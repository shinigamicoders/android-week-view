package com.alamkanak.weekview.sample;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class AddEventDialog extends DialogFragment {

    /*
     * View fields
     */
    private EditText mEventTitleEditText;
    private EditText mEventDurationEditText;
    private Button mSaveButton;


    private Context mContext;
    private Calendar mCalendar;

    OnSaveClickListener mCallback;

    public interface OnSaveClickListener {
        void onSaveClicked(WeekViewEvent event);
    }

    public void setOnSaveClickListener(OnSaveClickListener onSaveClickListener) {
        mCallback = onSaveClickListener;
    }

    static AddEventDialog newInstance(long timeInMillis) {
        AddEventDialog addEventDialog = new AddEventDialog();
        Bundle args = new Bundle();
        args.putLong("time_in_millis", timeInMillis);
        addEventDialog.setArguments(args);
        return addEventDialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            window.setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(style, theme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_event, container, false);
        getDialog().setTitle("New event");
        mContext = getActivity();

        long timeInMillis = getArguments().getLong("time_in_millis");
        mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(timeInMillis);

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        mEventTitleEditText = (EditText) view.findViewById(R.id.eventTitleEditText);
        mEventDurationEditText = (EditText) view.findViewById(R.id.durationEditText);
        mSaveButton = (Button) view.findViewById(R.id.saveButton);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEvent();
            }
        });
    }

    private void addEvent() {

        String eventTitle = mEventTitleEditText.getText().toString();
        int duration = Integer.parseInt(mEventDurationEditText.getText().toString());

        Calendar startTime = mCalendar;
        Calendar endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR, duration);
        WeekViewEvent event = new WeekViewEvent(1, eventTitle, startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_05));

        mCallback.onSaveClicked(event);
        getDialog().cancel();
    }

}
